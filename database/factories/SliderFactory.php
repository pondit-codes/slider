<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Slider::class, function (Faker $faker) {

    return [
        'title' => $faker->realText(100),
        'image' =>$faker->image(public_path('uploads/slider-image'), 900, 400, 'nature', false),
        'is_active' => true
    ];
});
