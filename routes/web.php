<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Route::get('/sliders', 'SlidersController@index');
//Route::get('/sliders/create', 'SlidersController@create');
//Route::get('/sliders/{id}', 'SlidersController@show');
//Route::post('/sliders', 'SlidersController@store');
//Route::get('/sliders/{id}/edit', 'SlidersController@edit');
//Route::put('/sliders/{id}', 'SlidersController@update');
//Route::delete('/sliders/{id}', 'SlidersController@destroy');
Route::resource('sliders', 'SlidersController');//resource route


