<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Sliders</title>
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
</head>
<body>

<div class="container">
    <a href="{{ url('/sliders/create') }}" class="btn btn-primary">Add New</a>

    @if(session('message'))
        <span class="alert alert-success">{{ session('message') }}</span>
     @endif

    <table class="table table-bordered">
        <thead>
            <tr>
                <th>Title</th>
                <th>Image</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($sliders as $slider)
                <tr>
                    <td>{{ $slider->title }}</td>
                    <td><img src="{{ asset('uploads/slider-image/'.$slider->image) }}" width="200" alt=""></td>
                    <td>{{ $slider->is_active ? 'Yes' : 'No' }}</td>
                    <td>
                        <a href="{{ url('sliders/'.$slider->id) }}">Show</a> |
                        <a href="{{ url('sliders/'.$slider->id.'/edit') }}"> Edit </a>
                        |
                        <form action="{{ url('sliders/'.$slider->id) }}" method="post" style="display: inline">
                            @csrf
                            {{ method_field('delete') }}
                            <button type="submit" onclick="return confirm('Are You Sure Want To Delete ?')">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>

</body>
</html>