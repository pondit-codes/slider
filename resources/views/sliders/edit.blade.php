<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Sliders</title>
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
</head>
<body>

<div class="container">
    <a href="{{ url('/sliders') }}" class="btn btn-primary">List</a>

    <form action="{{ url('sliders/'.$slider->id) }}" method="post" enctype="multipart/form-data">
        @csrf
        {{ method_field('put') }}
        <div class="form-group">
            <input type="text" class="form-control" name="title" value="{{ $slider->title }}" placeholder="Enter Title">
        </div>
        <div class="form-group">
            <img src="{{ asset('uploads/slider-image/'.$slider->image) }}" width="150">
            <input type="file" class="form-control" name="image">
        </div>
        <div class="form-group">
            <label>Is Active? </label>
            @if($slider->is_active)
                <input type="radio" name="is_active" value="1" checked>Yes
                <input type="radio" name="is_active" value="0">No
            @else
                <input type="radio" name="is_active" value="1">Yes
                <input type="radio" name="is_active" value="0" checked>No
            @endif
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-success">Update</button>
        </div>
    </form>
</div>

</body>
</html>